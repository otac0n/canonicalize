Canonicalize
=======

To use:

    cd ~/Projects &&
    git clone https://bitbucket.org/otac0n/canonicalize.git Canonicalize &&
    cd Canonicalize/.git_template &&
    git config --global init.templatedir "$PWD"
