﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Normalize
{
    using System.Xml.Linq;

    public class ConfigNormalizer
    {
        public static void Normalize(string sourcePath, string destinationPath)
        {
            var document = XDocument.Load(sourcePath);

            var configuration = document.Root;
            var configSections = configuration.Element("configSections");
            if (configSections != null)
            {
                configSections.Remove();
                SortChildren(configSections);
            }

            var comments = document
                .DescendantNodes()
                .OfType<XComment>()
                .Select(c => new { Comment = c, Next = c.NextNode })
                .Where(r => r.Next != null)
                .OrderByDescending(c => c.Comment, new XNodeDocumentOrderComparer())
                .ToArray();

            comments.Select(r => r.Comment).Remove();

            SortChildren(configuration);

            if (configSections != null)
                configuration.AddFirst(configSections);

            foreach (var r in comments)
            {
                //r.Next.AddBeforeSelf(r.Comment);
            }

            document.Save(destinationPath);
        }

        private static void SortChildren(XElement parent)
        {
            foreach (var child in parent.Elements())
            {
                SortChildren(child);
            }

            parent.ReplaceAttributes(parent.Attributes().OrderBy(e => e.Name, AttributeNameComparer.Instance));
            SortFirst(parent.Attribute("key"));
            SortFirst(parent.Attribute("name"));
            SortFirst(parent.Attribute("id"));

            parent.ReplaceNodes(
                parent.Nodes()
                .OrderBy(e => e is XElement ? ((XElement)e).Name.LocalName : "", NodeNameComparer.Instance)
                .ThenBy(Attr("name"))
                .ThenBy(Attr("key"))
            );
        }

        private static void SortFirst(XAttribute obj)
        {
            if (obj != null)
            {
                var parent = obj.Parent;
                obj.Remove();
                parent.ReplaceAttributes(obj, parent.Attributes());
                //parent.AddFirst(obj);
            }
        }

        private static Func<XNode, string> Attr(string name)
        {
            return node =>
            {
                var element = node as XElement;
                if (element == null)
                {
                    return "";
                }

                var attr = element.Attribute(name);
                if (attr == null)
                {
                    return "";
                }
                else
                {
                    return attr.Value;
                }
            };
        }
    }

    internal class AttributeNameComparer : IComparer<XName>
    {
        private XNamespace xdt = "http://schemas.microsoft.com/XML-Document-Transform";

        public static readonly AttributeNameComparer Instance = new AttributeNameComparer();

        public int Compare(XName x, XName y)
        {
            var xdtCompare = (x.Namespace == xdt).CompareTo(y.Namespace == xdt);
            if (xdtCompare != 0)
            {
                return xdtCompare;
            }

            if (x.LocalName.StartsWith("system") && !y.LocalName.StartsWith("system"))
            {
                return -1;
            }
            else if (!x.LocalName.StartsWith("system") && y.LocalName.StartsWith("system"))
            {
                return +1;
            }

            var targetFrameworkCompare = (x.LocalName == "targetFramework").CompareTo(y.LocalName == "targetFramework");
            if (targetFrameworkCompare != 0)
            {
                return targetFrameworkCompare;
            }

            return StringComparer.OrdinalIgnoreCase.Compare(x.LocalName, y.LocalName);
        }
    }

    internal class NodeNameComparer : IComparer<string>
    {
        public static readonly NodeNameComparer Instance = new NodeNameComparer();

        public int Compare(string x, string y)
        {
            var appSettings = x.Equals("appSettings").CompareTo(y.Equals("appSettings"));
            if (appSettings != 0)
            {
                return -appSettings;
            }

            var connectionStrings = x.Equals("connectionStrings").CompareTo(y.Equals("connectionStrings"));
            if (connectionStrings != 0)
            {
                return -connectionStrings;
            }

            if (x.StartsWith("system") && !y.StartsWith("system"))
            {
                return -1;
            }
            else if (!x.StartsWith("system") && y.StartsWith("system"))
            {
                return +1;
            }

            var runtime = x.Equals("runtime").CompareTo(y.Equals("runtime"));
            if (runtime != 0)
            {
                return -runtime;
            }

            if (x == "remove" || x == "clear")
            {
                x = "add";
            }

            if (y == "remove" || y == "clear")
            {
                y = "add";
            }

            return StringComparer.OrdinalIgnoreCase.Compare(x, y);
        }
    }
}
