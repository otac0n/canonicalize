﻿using System;
using System.IO;

namespace Normalize
{
    public static class SlnNormalizer
    {
        public static void Normalize(string sourcePath, string destinationPath)
        {
            var content = File.ReadAllText(sourcePath);

            string newContent;
            try
            {
                newContent = new SolutionParser().Parse(content).ToString();
            }
            catch (FormatException)
            {
                newContent = content;
            }

            if (content != newContent)
            {
                File.WriteAllText(destinationPath, newContent);
            }
        }
    }
}
