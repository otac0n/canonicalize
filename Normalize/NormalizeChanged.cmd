@echo off
set scriptdir=%~dp0
for /F %%c in ('hg parents ^| find "changeset" /C') do set parents=%%c
if "%parents%"=="2" exit 0
for /F %%i in ('hg status -amn') do %scriptdir%normalize.exe %%i
exit 0
