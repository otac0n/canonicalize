﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace Normalize
{
    public static class CsprojNormalizer
    {
        private static XNamespace msbuild = "http://schemas.microsoft.com/developer/msbuild/2003";

        public static void Normalize(string sourcePath, string destinationPath)
        {
            var document = XDocument.Load(sourcePath);

            var items = new List<XNode>();
            var node = (from i in document.Root.Elements(msbuild + "ItemGroup")
                        select i.PreviousNode).FirstOrDefault();

            foreach (var node2 in document.Root.Nodes().ToList())
            {
                ProcessChild(node2, items);
            }

            var content = ProcessItems(items);
            if (node != null)
            {
                node.AddAfterSelf(content);
            }
            else
            {
                document.Root.AddFirst(content);
            }

            foreach (var element in document.Descendants().Where(e => !e.IsEmpty).Where(ShouldBeEmpty).ToList())
            {
                element.RemoveNodes();
            }

            while (true)
            {
                var toRemove = (from e in document.Descendants()
                                where e.IsEmpty
                                where !e.Name.LocalName.StartsWith("IISExpress")
                                where e.Parent.Name == msbuild + "PropertyGroup" || e.Name == msbuild + "PropertyGroup"
                                select e).ToList();

                if (toRemove.Count == 0)
                {
                    break;
                }

                foreach (var element in toRemove)
                {
                    element.Remove();
                }
            }

            document.Save(destinationPath);
        }

        private static bool ShouldBeEmpty(XElement element)
        {
            if (element.IsEmpty)
            {
                return true;
            }

            var nodes = element.Nodes().ToList();
            if (nodes.Count == 0)
            {
                return true;
            }
            else if (nodes.Count > 1)
            {
                return false;
            }

            var node = nodes[0];
            if (node.NodeType == XmlNodeType.Text && string.IsNullOrWhiteSpace(((XText)node).Value))
            {
                return true;
            }

            return false;
        }

        private static void ProcessChild(XNode child, List<XNode> items)
        {
            if (child is XComment)
            {
                var comment = child as XComment;
                if (comment.Value.StartsWith(" To modify your build process,"))
                {
                    child.Remove();
                }
            }
            else if (child is XElement)
            {
                var element = child as XElement;
                if (!(element.Name != msbuild + "ItemGroup" || element.HasAttributes))
                {
                    element.Remove();
                    items.AddRange(element.Nodes());
                }
            }
        }

        private static List<XElement> ProcessItems(List<XNode> items)
        {
            var content = new List<XNode>();
            var references = new List<XElement>();
            var projectReferences = new List<XElement>();
            var projectContents = new List<XElement>();
            var folders = new List<XElement>();
            var bootstrapperPackages = new List<XElement>();

            foreach (var node in items)
            {
                if (!(node is XElement))
                {
                    content.Add(node);
                }
                else
                {
                    var item = (XElement)node;
                    if (item.Name == msbuild + "Reference")
                    {
                        var include = (string)item.Attribute("Include");
                        if (include != null)
                        {
                            var versionIndex = include.IndexOf(", Version=");
                            if (versionIndex >= 0)
                            {
                                var specificVersion = item.Element(msbuild + "SpecificVersion");
                                if ((string)specificVersion == "False")
                                {
                                    specificVersion.Remove();
                                    item.SetAttributeValue("Include", include.Substring(0, versionIndex));
                                }

                                var hintPath = item.Element(msbuild + "HintPath");
                                if (hintPath != null)
                                {
                                    var @private = item.Element(msbuild + "Private");
                                    if (@private == null)
                                    {
                                        item.Add(new XElement(msbuild + "Private", "True"));
                                    }

                                }
                            }
                        }

                        references.Add(item);
                    }
                    else if (item.Name == msbuild + "Build" ||
                             item.Name == msbuild + "Compile" ||
                             item.Name == msbuild + "Content" ||
                             item.Name == msbuild + "EmbeddedResource" ||
                             item.Name == msbuild + "None" ||
                             item.Name == msbuild + "WebReferences")
                    {
                        projectContents.Add(item);
                    }
                    else if (item.Name == msbuild + "Folder")
                    {
                        folders.Add(item);
                    }
                    else if (item.Name == msbuild + "ProjectReference")
                    {
                        projectReferences.Add(item);
                    }
                    else if (item.Name == msbuild + "BootstrapperPackage")
                    {
                        bootstrapperPackages.Add(item);
                    }
                    else
                    {
                        content.Add(item);
                    }
                }
            }

            var results = new List<XElement>();

            if (references.Count > 0)
            {
                references.Sort((a, b) =>
                {
                    var aName = (string)a.Attribute("Include");
                    var aPath = aName.Split(',')[0].Replace(".", @"\");

                    var bName = (string)b.Attribute("Include");
                    var bPath = bName.Split(',')[0].Replace(".", @"\");

                    return ComparePaths(aPath, bPath);
                });
                results.Add(new XElement(msbuild + "ItemGroup", references));
            }

            if (content.Count > 0)
            {
                results.Add(new XElement(msbuild + "ItemGroup", content));
            }

            if (bootstrapperPackages.Count > 0)
            {
                results.Add(new XElement(msbuild + "ItemGroup", bootstrapperPackages));
            }

            if (projectContents.Count > 0)
            {
                projectContents.Sort((a, b) =>
                {
                    var aPath = (string)a.Attribute("Include");
                    var aElement = a.Element(msbuild + "Link");
                    if (aElement != null)
                    {
                        aPath = (string)aElement;
                    }

                    var bPath = (string)b.Attribute("Include");
                    var bElement = b.Element(msbuild + "Link");
                    if (bElement != null)
                    {
                        bPath = (string)bElement;
                    }

                    return ComparePaths(aPath, bPath);
                });
                results.Add(new XElement(msbuild + "ItemGroup", projectContents));
            }

            if (folders.Count > 0)
            {
                folders.Sort((a, b) => ComparePaths((string)a.Attribute("Include"), (string)b.Attribute("Include")));
                results.Add(new XElement(msbuild + "ItemGroup", folders));
            }

            if (projectReferences.Count > 0)
            {
                projectReferences.Sort((a, b) => ComparePaths((string)a.Attribute("Include"), (string)b.Attribute("Include")));
                results.Add(new XElement(msbuild + "ItemGroup", projectReferences));
            }

            return results;
        }

        private static int ComparePaths(string aPath, string bPath)
        {
            string[] aParts = aPath.ToUpperInvariant().Split(@"\".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string[] bParts = bPath.ToUpperInvariant().Split(@"\".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            int num = 0;
            for (int j = 0; j < aParts.Length && j < bParts.Length && num == 0; j++)
            {
                num = aParts[j].CompareTo(bParts[j]);
            }

            if (num == 0)
            {
                num = aParts.Length.CompareTo(bParts.Length);
            }

            return num;
        }
    }
}
