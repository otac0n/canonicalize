﻿namespace Normalize
{
    using System.IO;

    public class Program
    {
        public static void Main(string[] args)
        {
            foreach (var arg in args)
            {
                var source = arg;
                var dest = arg;

                if (arg.Contains("="))
                {
                    var split = arg.Split('=');
                    source = split[0];
                    dest = split[1];
                }

                switch (Path.GetExtension(source).ToLower())
                {
                    case ".cs":
                        if (!source.ToLower().EndsWith(".designer.cs"))
                        {
                            CsNormalizer.Normalize(source, dest);
                        }

                        break;

                    case ".cshtml":
                        CshtmlNormalizer.Normalize(source, dest);
                        break;

                    case ".csproj":
                    case ".sqlproj":
                        CsprojNormalizer.Normalize(source, dest);
                        break;

                    case ".config":
                        ConfigNormalizer.Normalize(source, dest);
                        break;

                    case ".js":
                        JsNormalizer.Normalize(source, dest);
                        break;

                    case ".less":
                        LessNormalizer.Normalize(source, dest);
                        break;

                    case ".sln":
                        SlnNormalizer.Normalize(source, dest);
                        break;
                }
            }
        }
    }
}
