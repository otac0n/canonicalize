﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Normalize
{
    public class LessNormalizer
    {
        public static void Normalize(string sourcePath, string destinationPath)
        {
            var source = File.ReadAllBytes(sourcePath);

            string text;
            using (var stream = new MemoryStream(source))
            using (var reader = new StreamReader(stream, true))
            {
                text = reader.ReadToEnd();
            }

            var lines = new List<string>(Regex.Split(text.Trim(), @"\r\n?|\n\r?"));
            for (int i = 0; i < lines.Count; i++)
            {
                lines[i] = lines[i].TrimEnd();
            }

            bool previousWasBlank = false;
            for (int i = 0; i < lines.Count; )
            {
                if (lines[i] == "")
                {
                    if (previousWasBlank)
                    {
                        lines.RemoveAt(i);
                        continue;
                    }

                    previousWasBlank = true;
                }
                else
                {
                    previousWasBlank = false;
                }

                i++;
            }

            lines.Add("");

            var result = string.Join("\r\n", lines);

            var preamble = Encoding.UTF8.GetPreamble();
            var bytes = Encoding.UTF8.GetBytes(result);
            var allBytes = new byte[preamble.Length + bytes.Length];
            preamble.CopyTo(allBytes, 0);
            bytes.CopyTo(allBytes, preamble.Length);

            var binarySame = allBytes.Length == source.Length;
            for (int i = 0; binarySame && i < source.Length; i++)
            {
                binarySame &= source[i] == allBytes[i];
            }

            if (binarySame && sourcePath == destinationPath)
            {
                return;
            }

            File.WriteAllBytes(destinationPath, allBytes);
        }
    }
}
