﻿// -----------------------------------------------------------------------
// <copyright file="AppConfigCanonicalizer.cs" company="(none)">
//   Copyright © 2015 John Gietzen.  All Rights Reserved.
//   This source is subject to the MIT license.
//   Please see license.md for more information.
// </copyright>
// -----------------------------------------------------------------------

namespace Canonicalize
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    public class AppConfigCanonicalizer : ICanonicalizer
    {
        public void Canonicalize(Stream input, Stream output)
        {
            var document = XDocument.Load(input);

            document
                .DescendantNodes()
                .OfType<XComment>()
                .ToList()
                .Remove();

            var configuration = document.Root;
            SortChildren(configuration);

            var configSections = configuration.Element("configSections");
            if (configSections != null)
            {
                configSections.Remove();
                configuration.AddFirst(configSections);
            }

            document.Save(output);
        }

        private static Func<XNode, string> Attr(string name)
        {
            return node =>
            {
                var element = node as XElement;
                if (element == null)
                {
                    return "";
                }

                var attr = element.Attribute(name);
                if (attr == null)
                {
                    return "";
                }
                else
                {
                    return attr.Value;
                }
            };
        }

        private static void SortChildren(XElement parent)
        {
            foreach (var child in parent.Elements())
            {
                SortChildren(child);
            }

            SortFirst(parent.Attribute("key"));
            SortFirst(parent.Attribute("name"));
            SortFirst(parent.Attribute("id"));

            parent.ReplaceNodes(
                parent.Nodes()
                .OrderBy(e => e is XElement ? ((XElement)e).Name.LocalName : "", NodeNameComparer.Instance)
                .ThenBy(Attr("name"))
                .ThenBy(Attr("key")));
        }

        private static void SortFirst(XAttribute obj)
        {
            if (obj != null)
            {
                var parent = obj.Parent;
                obj.Remove();
                parent.ReplaceAttributes(obj, parent.Attributes());
            }
        }
    }

    internal class NodeNameComparer : IComparer<string>
    {
        public static readonly NodeNameComparer Instance = new NodeNameComparer();

        public int Compare(string x, string y)
        {
            var appSettings = -x.Equals("appSettings").CompareTo(y.Equals("appSettings"));
            if (appSettings != 0)
            {
                return appSettings;
            }

            var connectionStrings = -x.Equals("connectionStrings").CompareTo(y.Equals("connectionStrings"));
            if (connectionStrings != 0)
            {
                return connectionStrings;
            }

            if (x.StartsWith("system.") && !y.StartsWith("system."))
            {
                return -1;
            }
            else if (!x.StartsWith("system.") && y.StartsWith("system."))
            {
                return +1;
            }

            var runtime = x.Equals("runtime").CompareTo(y.Equals("runtime"));
            if (runtime != 0)
            {
                return -runtime;
            }

            if (x == "remove" || x == "clear")
            {
                x = "add";
            }

            if (y == "remove" || y == "clear")
            {
                y = "add";
            }

            return StringComparer.OrdinalIgnoreCase.Compare(x, y);
        }
    }
}
