// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="(none)">
//   Copyright © 2014 John Gietzen.  All Rights Reserved.
//   This source is subject to the MIT license.
//   Please see license.md for more information.
// </copyright>
// -----------------------------------------------------------------------

namespace Canonicalize
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    internal class Program
    {
        public static int Main(string[] args)
        {
            var options = ParseOptions(args);
            if (options == null)
            {
                return 1;
            }

            foreach (var file in options.Files)
            {
                var canonicalizer = GetCanonicalizer(file);

                var input = new MemoryStream();
                var output = new MemoryStream();

                using (var @in = options.Pipe ? Console.OpenStandardInput() : File.OpenRead(file))
                {
                    @in.CopyTo(input);
                    input.Seek(0, SeekOrigin.Begin);
                }

                canonicalizer.Canonicalize(input, output);
                input.Seek(0, SeekOrigin.Begin);
                output.Seek(0, SeekOrigin.Begin);

                var changed = DetectChange(input, output);

                if (options.Check)
                {
                    if (changed)
                    {
                        Console.WriteLine(file);
                    }
                }
                else if (changed || options.Pipe)
                {
                    using (var @out = options.Pipe ? Console.OpenStandardOutput() : File.Open(file, FileMode.Create))
                    {
                        output.CopyTo(@out);
                        @out.Flush();
                    }
                }
            }

            return 0;
        }

        private static bool DetectChange(MemoryStream input, MemoryStream output)
        {
            var changed = false;

            while (true)
            {
                var i = input.ReadByte();
                var o = output.ReadByte();

                if (i != o)
                {
                    changed = true;
                    break;
                }

                if (i == -1 || o == -1)
                {
                    break;
                }
            }

            input.Seek(0, SeekOrigin.Begin);
            output.Seek(0, SeekOrigin.Begin);

            return changed;
        }

        private static ICanonicalizer GetCanonicalizer(string file)
        {
            var name = Path.GetFileName(file);
            var extension = Path.GetExtension(file).ToLower();

            switch (extension)
            {
                case ".config":
                    if (name.Equals("packages.config", StringComparison.CurrentCultureIgnoreCase))
                    {
                        return new PackagesConfigCanonicalizer();
                    }
                    else if (name.StartsWith("App.", StringComparison.CurrentCultureIgnoreCase) ||
                             name.StartsWith("Web.", StringComparison.CurrentCultureIgnoreCase))
                    {
                        return new AppConfigCanonicalizer();
                    }

                    break;

                case ".csproj":
                case ".fsproj":
                case ".sqlproj":
                case ".vbproj":
                    return new ProjCanonicalizer();

                case ".sln":
                    return new SlnCanonicalizer();
            }

            return new NullCanonicalizer();
        }

        private static Options ParseOptions(string[] args)
        {
            var options = new Options();

            var stop = false;
            var error = false;
            foreach (var arg in args)
            {
                if (!stop && arg.StartsWith("--"))
                {
                    switch (arg.ToLower())
                    {
                        case "--": stop = true; break;
                        case "--check": options.Check = true; break;
                        case "--pipe": options.Pipe = true; break;
                        default:
                            Console.Error.WriteLine("Unrecognized switch '" + arg + "'.");
                            error = true;
                            break;
                    }

                    continue;
                }

                options.Files.Add(arg);
            }

            if (options.Files.Count == 0)
            {
                Console.Error.WriteLine("You must specify at least one file.");
                error = true;
            }
            else if (options.Pipe && options.Files.Count > 1)
            {
                Console.Error.WriteLine("You may only specify a single filename with the --pipe switch.");
                error = true;
            }

            if (error)
            {
                Console.WriteLine();
                Console.WriteLine("Usage:");
                Console.WriteLine("    canonicalize.exe [--check] [--pipe] FILE1 [FILE2 ...]");
                Console.WriteLine();
                return null;
            }
            else
            {
                return options;
            }
        }

        private class Options
        {
            public Options()
            {
                this.Files = new List<string>();
            }

            public bool Check { get; set; }

            public List<string> Files { get; private set; }

            public bool Pipe { get; set; }
        }
    }
}
