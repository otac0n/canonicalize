@namespace Canonicalize
@classname SolutionParser
@accessibility internal
@using System.Linq

solution <Solution>
  = (nl / ws)* h:header s:solutionEntry* p:project* g:global eof {
      new Solution(h, s, p, g)
  }

solutionEntry <SolutionEntry>
  = key:("" (!(ws "=" / "(") .)*) ws "=" ws val:("" (!nl .)*) nl {
      new SolutionEntry(key, val)
  }

header <Header>
  = "Microsoft Visual Studio Solution File, Format Version " v:version nl c:(i:("#" (!nl .)* / ws / ) nl { i })* {
      new Header(v, c)
  }

version
  = [0-9]+ ("." [0-9]+)*

project <Project>
  = "Project(\"" id:("" [^"]+) "\")" ws "=" ws a:("\"" i:("" [^"]+) "\"" { i })<1,,"," ws> nl
    s:projectSection*
    "EndProject" nl
  {
      new Project(id, a, s)
  }

projectSection <ProjectSection>
  = "\tProjectSection(" id:("" [^)]+) ")" ws "=" ws v:("" (!nl [^ ])+) nl
    e:sectionEntry*
    "\tEndProjectSection" nl
  {
      new ProjectSection(id, v, e)
  }

global <Global>
  = "Global" nl
    s:globalSection*
    "EndGlobal" nl
  {
      new Global(s)
  }

globalSection <GlobalSection>
  = "\tGlobalSection(" id:("" [^)]+) ")" ws "=" ws v:("" (!nl [^ ])+) nl
    e:sectionEntry*
    "\tEndGlobalSection" nl
  {
      new GlobalSection(id, v, e)
  }

sectionEntry <SectionEntry>
  = "\t\t" key:("" (!(ws "=") .)*) ws "=" ws val:("" (!nl .)*) nl {
      new SectionEntry(key, val)
  }

ws = "" [ ]+
nl = "\r\n" / "\n"
eof = "" !.
