﻿// -----------------------------------------------------------------------
// <copyright file="PackagesConfigCanonicalizer.cs" company="(none)">
//   Copyright © 2015 John Gietzen.  All Rights Reserved.
//   This source is subject to the MIT license.
//   Please see license.md for more information.
// </copyright>
// -----------------------------------------------------------------------

namespace Canonicalize
{
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    public class PackagesConfigCanonicalizer : ICanonicalizer
    {
        public void Canonicalize(Stream input, Stream output)
        {
            var document = XDocument.Load(input);

            var packages = document.Root;
            packages.ReplaceNodes(
                packages.Elements().OrderBy(n => (string)n.Attribute("id")));

            document.Save(output);
        }
    }
}
