﻿// -----------------------------------------------------------------------
// <copyright file="NullCanonicalizer.cs" company="(none)">
//   Copyright © 2014 John Gietzen.  All Rights Reserved.
//   This source is subject to the MIT license.
//   Please see license.md for more information.
// </copyright>
// -----------------------------------------------------------------------

namespace Canonicalize
{
    using System.IO;

    public class NullCanonicalizer : ICanonicalizer
    {
        public void Canonicalize(Stream input, Stream output)
        {
            input.CopyTo(output);
        }
    }
}
