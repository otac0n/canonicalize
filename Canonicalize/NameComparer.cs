﻿// -----------------------------------------------------------------------
// <copyright file="CsprojCanonicalizer.cs" company="(none)">
//   Copyright © 2014 John Gietzen.  All Rights Reserved.
//   This source is subject to the MIT license.
//   Please see license.md for more information.
// </copyright>
// -----------------------------------------------------------------------

namespace Canonicalize
{
    using System;
    using System.IO;

    internal static class NameComparer
    {

        private static char[] PathSeparators = new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar };

        public static int ComparePaths(string aPath, string bPath)
        {
            var aParts = aPath.ToUpperInvariant().Split(PathSeparators, StringSplitOptions.RemoveEmptyEntries);
            var bParts = bPath.ToUpperInvariant().Split(PathSeparators, StringSplitOptions.RemoveEmptyEntries);

            var num = 0;
            for (var j = 0; j < aParts.Length && j < bParts.Length; j++)
            {
                if (aParts.Length != bParts.Length)
                {
                    if (j == aParts.Length - 1)
                    {
                        return 1;
                    }
                    else if (j == bParts.Length - 1)
                    {
                        return -1;
                    }
                }

                if ((num = aParts[j].CompareTo(bParts[j])) != 0)
                {
                    return num;
                }
            }

            return 0;
        }

        public static int CompareNamespaces(string aNamespace, string bNamespace, char separator = '.')
        {
            var aParts = aNamespace.ToUpperInvariant().Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);
            var bParts = bNamespace.ToUpperInvariant().Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);

            var num = 0;
            for (var j = 0; j < aParts.Length && j < bParts.Length; j++)
            {
                if ((num = aParts[j].CompareTo(bParts[j])) != 0)
                {
                    return num;
                }
            }

            return aParts.Length.CompareTo(bParts.Length);
        }
    }
}
