﻿// -----------------------------------------------------------------------
// <copyright file="CsprojCanonicalizer.cs" company="(none)">
//   Copyright © 2014 John Gietzen.  All Rights Reserved.
//   This source is subject to the MIT license.
//   Please see license.md for more information.
// </copyright>
// -----------------------------------------------------------------------

namespace Canonicalize
{
    using System;
    using System.IO;

    public class SlnCanonicalizer : ICanonicalizer
    {
        public void Canonicalize(Stream input, Stream output)
        {
            var content = new StreamReader(input).ReadToEnd();

            string newContent;
            try
            {
                newContent = new SolutionParser().Parse(content).ToString();
            }
            catch (FormatException)
            {
                newContent = content;
            }

            var writer = new StreamWriter(output);
            writer.Write(newContent);
            writer.Flush();
        }
    }
}
