﻿using System.IO;

namespace Canonicalize
{
    internal interface ICanonicalizer
    {
        void Canonicalize(Stream input, Stream output);
    }
}
