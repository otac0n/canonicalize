namespace Canonicalize
{
    using System.Collections.Generic;
    using System.Linq;

    internal class Global
    {
        public Global(IEnumerable<GlobalSection> sections)
        {
            this.Sections = sections.ToList().AsReadOnly();
        }

        private Global()
        {
        }

        public IList<GlobalSection> Sections { get; private set; }

        public Global Reorder(IList<string> projectOrder)
        {
            return new Global(this.Sections.Select(s => s.Reorder(projectOrder)));
        }

        public override string ToString()
        {
            return "Global\r\n" + string.Concat(this.Sections) + "EndGlobal\r\n";
        }
    }

    internal class GlobalSection
    {
        public GlobalSection(string id, string value, IEnumerable<SectionEntry> entries)
        {
            this.Id = id;
            this.Value = value;
            this.Entries = entries.OrderBy(e => e.Key).ToList().AsReadOnly();
        }

        private GlobalSection()
        {
        }

        public IList<SectionEntry> Entries { get; private set; }

        public string Id { get; private set; }

        public string Value { get; private set; }

        public GlobalSection Reorder(IList<string> projectOrder)
        {
            if (this.Id == "ProjectConfigurationPlatforms")
            {
                return new GlobalSection
                {
                    Id = this.Id,
                    Value = this.Value,
                    Entries = this.Entries.OrderBy(e => projectOrder.IndexOf(e.Key.Substring(0, 38))).ThenBy(e => e.Key).ToList().AsReadOnly(),
                };
            }
            else if (this.Id == "NestedProjects")
            {
                return new GlobalSection
                {
                    Id = this.Id,
                    Value = this.Value,
                    Entries = this.Entries.OrderBy(e => projectOrder.IndexOf(e.Key.ToUpper())).ToList().AsReadOnly(),
                };
            }
            else
            {
                return this;
            }
        }

        public override string ToString()
        {
            return "\tGlobalSection(" + this.Id + ") = " + this.Value + "\r\n" + string.Concat(this.Entries) + "\tEndGlobalSection\r\n";
        }
    }

    internal class Header
    {
        public Header(string version, IEnumerable<string> comments)
        {
            this.Version = version;
            this.Comments = comments.Where(c => !string.IsNullOrWhiteSpace(c)).ToList().AsReadOnly();
        }

        public IList<string> Comments { get; private set; }

        public string Version { get; private set; }

        public override string ToString()
        {
            return "Microsoft Visual Studio Solution File, Format Version " + this.Version + "\r\n" + string.Concat(this.Comments.Select(c => c + "\r\n"));
        }
    }

    internal class Project
    {
        public Project(string id, IList<string> args, IEnumerable<ProjectSection> sections)
        {
            this.Id = id;
            this.Args = args.ToList().AsReadOnly();
            this.Sections = sections.ToList().AsReadOnly();
        }

        public IList<string> Args { get; private set; }

        public string Id { get; private set; }

        public IList<ProjectSection> Sections { get; private set; }

        public override string ToString()
        {
            return "Project(\"" + this.Id + "\") = " + string.Join(", ", this.Args.Select(a => "\"" + a + "\"")) + "\r\n" + string.Concat(this.Sections) + "EndProject\r\n";
        }
    }

    internal class ProjectSection
    {
        public ProjectSection(string id, string value, IEnumerable<SectionEntry> entries)
        {
            this.Id = id;
            this.Value = value;
            this.Entries = entries.OrderBy(e => e.Key).ToList().AsReadOnly();
        }

        public IList<SectionEntry> Entries { get; private set; }

        public string Id { get; private set; }

        public string Value { get; private set; }

        public override string ToString()
        {
            return "\tProjectSection(" + this.Id + ") = " + this.Value + "\r\n" + string.Concat(this.Entries) + "\tEndProjectSection\r\n";
        }
    }

    internal class SectionEntry
    {
        public SectionEntry(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }

        public string Key { get; private set; }

        public string Value { get; private set; }

        public override string ToString()
        {
            return "\t\t" + this.Key + " = " + this.Value + "\r\n";
        }
    }

    internal class SolutionEntry
    {
        public SolutionEntry(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }

        public string Key { get; private set; }

        public string Value { get; private set; }

        public override string ToString()
        {
            return this.Key + " = " + this.Value + "\r\n";
        }
    }

    internal class Solution
    {
        public Solution(Header header, IEnumerable<SolutionEntry> entries, IEnumerable<Project> projects, Global global)
        {
            this.Header = header;
            this.Entries = entries.ToList().AsReadOnly();
            this.Projects = projects.OrderBy(p => p.Id.ToUpper() != "{2150E333-8FDC-42A3-9474-1A3956D46DE8}").ThenBy(p => p.Args[0], Comparer<string>.Create(CompareNames)).ToList().AsReadOnly();
            this.Global = global.Reorder(this.Projects.Select(p => p.Args[2].ToUpper()).ToList());
        }

        public Global Global { get; private set; }

        public Header Header { get; private set; }

        public IList<SolutionEntry> Entries { get; private set; }

        public IList<Project> Projects { get; private set; }

        public override string ToString()
        {
            return this.Header + string.Concat(this.Entries) + string.Concat(this.Projects) + this.Global;
        }

        private static int CompareNames(string aName, string bName)
        {
            aName = aName.ToUpperInvariant();
            bName = bName.ToUpperInvariant();

            var aParts = aName.Split(".".ToCharArray());
            var bParts = bName.Split(".".ToCharArray());

            var num = 0;
            for (var j = 0; j < aParts.Length && j < bParts.Length && num == 0; j++)
            {
                num = aParts[j].CompareTo(bParts[j]);
            }

            if (num == 0)
            {
                num = aParts.Length.CompareTo(bParts.Length);
            }

            return num;
        }
    }
}
