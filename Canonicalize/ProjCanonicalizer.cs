// -----------------------------------------------------------------------
// <copyright file="CsprojCanonicalizer.cs" company="(none)">
//   Copyright © 2014 John Gietzen.  All Rights Reserved.
//   This source is subject to the MIT license.
//   Please see license.md for more information.
// </copyright>
// -----------------------------------------------------------------------

namespace Canonicalize
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    public class ProjCanonicalizer : ICanonicalizer
    {
        private static XNamespace msbuild = "http://schemas.microsoft.com/developer/msbuild/2003";

        private static bool IsName(XName xName, string name, bool useMSBuild) => xName == MakeName(name, useMSBuild);

        private static XName MakeName(string name, bool useMSBuild) => useMSBuild ? msbuild + name : name;

        private static string GetItemElementPath(XElement item, bool useMSBuild) =>
            (string)item.Attribute("Link") ??
            (string)item.Element(MakeName("Link", useMSBuild)) ??
            (string)item.Attribute("Include") ??
            (string)item.Attribute("Update") ??
            (string)item.Attribute("Remove");

        public void Canonicalize(Stream input, Stream output)
        {
            var document = XDocument.Load(input);

            var items = new List<XNode>();

            var useMSBuild = document.Root.Name.Namespace == msbuild;

            var node = (from i in document.Root.Elements()
                        where IsName(i.Name, "ItemGroup", useMSBuild)
                        select i.PreviousNode).FirstOrDefault();

            foreach (var node2 in document.Root.Nodes().ToList())
            {
                ProcessChild(node2, items, useMSBuild);
            }

            var content = ProcessItems(items, useMSBuild);
            if (node != null)
            {
                node.AddAfterSelf(content);
            }
            else
            {
                document.Root.AddFirst(content);
            }

            foreach (var element in document.Descendants().Where(e => !e.IsEmpty).Where(ShouldBeEmpty).ToList())
            {
                element.RemoveNodes();
            }

            bool removed;
            do
            {
                removed = false;

                foreach (var element in document.Descendants().Where(e => e.IsEmpty).ToList())
                {
                    if (IsName(element.Parent.Name, "PropertyGroup", useMSBuild) || IsName(element.Name, "PropertyGroup", useMSBuild))
                    {
                        if (element.Name.LocalName.StartsWith("IISExpress"))
                        {
                            continue;
                        }
                    }
                    else if (IsName(element.Parent.Name, "WebProjectProperties", useMSBuild) && IsName(element.Name, "CustomServerUrl", useMSBuild))
                    {
                        element.Add(new XText(Environment.NewLine + "          "));
                        continue;
                    }
                    else
                    {
                        continue;
                    }

                    element.Remove();
                    removed = true;
                }
            }
            while (removed);

            document.Save(output);
        }

        private static void ProcessChild(XNode child, List<XNode> items, bool useMSBuild)
        {
            if (child is XComment)
            {
                var comment = child as XComment;
                if (comment.Value.StartsWith(" To modify your build process,"))
                {
                    child.Remove();
                }
            }
            else if (child is XElement)
            {
                var element = child as XElement;
                if (IsName(element.Name, "ItemGroup", useMSBuild) && !element.HasAttributes)
                {
                    element.Remove();
                    items.AddRange(element.Nodes());
                }
            }
        }

        private static List<XElement> ProcessItems(List<XNode> items, bool useMSBuild)
        {
            var content = new List<XNode>();
            var references = new List<XElement>();
            var packages = new List<XElement>();
            var projects = new List<XElement>();
            var otherContents = new List<XElement>();
            var projectContents = new List<XElement>();

            foreach (var node in items)
            {
                if (!(node is XElement))
                {
                    content.Add(node);
                }
                else
                {
                    var item = (XElement)node;
                    if (IsName(item.Name, "Reference", useMSBuild))
                    {
                        var include = (string)item.Attribute("Include");
                        if (include != null)
                        {
                            var versionIndex = include.IndexOf(", Version=");
                            if (versionIndex >= 0)
                            {
                                var specificVersion = item.Element(MakeName("SpecificVersion", useMSBuild));
                                if ((string)specificVersion == "False")
                                {
                                    specificVersion.Remove();
                                    item.SetAttributeValue("Include", include.Substring(0, versionIndex));
                                }
                            }
                        }

                        var hintPath = (string)item.Element(MakeName("HintPath", useMSBuild));
                        var name = (string)item.Element(MakeName("Name", useMSBuild));
                        if (hintPath != null || name != null)
                        {
                            var @private = item.Element(MakeName("Private", useMSBuild));
                            if (@private == null)
                            {
                                item.Add(new XElement(MakeName("Private", useMSBuild), "True"));
                            }
                        }

                        references.Add(item);
                    }
                    else if (IsName(item.Name, "PackageReference", useMSBuild))
                    {
                        packages.Add(item);
                    }
                    else if (IsName(item.Name, "ProjectReference", useMSBuild))
                    {
                        projects.Add(item);
                    }
                    else if (IsName(item.Name, "BootstrapperPackage", useMSBuild) ||
                             IsName(item.Name, "Folder", useMSBuild) ||
                             IsName(item.Name, "WebReferences", useMSBuild) ||
                             IsName(item.Name, "Analyzer", useMSBuild) ||
                             IsName(item.Name, "Service", useMSBuild))
                    {
                        otherContents.Add(item);
                    }
                    else
                    {
                        projectContents.Add(item);
                    }
                }
            }

            var results = new List<XElement>();

            if (references.Count > 0)
            {
                references.Sort((a, b) =>
                {
                    var aName = (string)a.Element(MakeName("Name", useMSBuild)) ?? (string)a.Attribute("Include");
                    aName = aName.Split(',')[0];

                    var bName = (string)b.Element(MakeName("Name", useMSBuild)) ?? (string)b.Attribute("Include");
                    bName = bName.Split(',')[0];

                    return NameComparer.CompareNamespaces(aName, bName);
                });

                results.Add(new XElement(MakeName("ItemGroup", useMSBuild), references));
            }

            if (packages.Count > 0)
            {
                packages.Sort((a, b) =>
                {
                    var aName = (string)a.Attribute("Include");
                    var bName = (string)b.Attribute("Include");

                    return NameComparer.CompareNamespaces(aName, bName);
                });

                results.Add(new XElement(MakeName("ItemGroup", useMSBuild), packages));
            }

            if (projects.Count > 0)
            {
                projects.Sort((a, b) =>
                {
                    var aName = (string)a.Element("Name") ?? Path.GetFileNameWithoutExtension((string)a.Attribute("Include"));
                    var bName = (string)b.Element("Name") ?? Path.GetFileNameWithoutExtension((string)b.Attribute("Include"));

                    return NameComparer.CompareNamespaces(aName, bName);
                });

                results.Add(new XElement(MakeName("ItemGroup", useMSBuild), projects));
            }

            if (content.Count > 0)
            {
                results.Add(new XElement(MakeName("ItemGroup", useMSBuild), content));
            }

            if (projectContents.Count > 0)
            {
                projectContents.Sort((a, b) =>
                {
                    var aPath = GetItemElementPath(a, useMSBuild);
                    var bPath = GetItemElementPath(b, useMSBuild);

                    return NameComparer.ComparePaths(aPath, bPath);
                });

                results.Add(new XElement(MakeName("ItemGroup", useMSBuild), projectContents));
            }

            if (otherContents.Count > 0)
            {
                var includeComparer = new IncludeComparer(useMSBuild);
                results.AddRange(from c in otherContents
                                 group c by c.Name.LocalName into g
                                 orderby g.Key
                                 select new XElement(MakeName("ItemGroup", useMSBuild), g.OrderBy(_ => _, includeComparer)));
            }

            return results;
        }

        private static bool ShouldBeEmpty(XElement element)
        {
            if (element.IsEmpty)
            {
                return true;
            }

            var nodes = element.Nodes().ToList();
            if (nodes.Count == 0)
            {
                return true;
            }
            else if (nodes.Count > 1)
            {
                return false;
            }

            var node = nodes[0];
            if (node.NodeType == XmlNodeType.Text && string.IsNullOrWhiteSpace(((XText)node).Value))
            {
                return true;
            }

            return false;
        }

        private class IncludeComparer : IComparer<XElement>
        {
            private readonly bool useMSBuild;

            public IncludeComparer(bool useMSBuild)
            {
                this.useMSBuild = useMSBuild;
            }

            public int Compare(XElement x, XElement y)
            {
                var xInclude = (GetItemElementPath(x, this.useMSBuild) ?? "").Split(';');
                var yInclude = (GetItemElementPath(y, this.useMSBuild) ?? "").Split(';');

                Array.Sort(xInclude, NameComparer.ComparePaths);
                Array.Sort(yInclude, NameComparer.ComparePaths);

                return NameComparer.ComparePaths(xInclude[0], yInclude[0]);
            }
        }
    }
}
